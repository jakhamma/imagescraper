//
//  ViewController.swift
//  ImageScraper
//
//  Created by brendan woods on 2016-07-16.
//  Copyright © 2016 brendan woods. All rights reserved.
//

import UIKit
import Kanna

class ViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet weak var imageCollection:UICollectionView!
    @IBOutlet weak var searchAddressTextField:UITextField!
    @IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    
    var imageLinks = [String]()
    var images = [UIImage]()
    var siteAddress:String = ""
    let reuseIdentifier = "imageCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //add the gesture recognizer to dismiss the keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        activityIndicator.hidden = true
        view.addGestureRecognizer(tap)
    }
    
    //Connect to the URL, collect data, and send to be parsed. If url is bad, display error
    private func setUpScrape(){
        let siteUrl = NSURL(string: siteAddress)
        let task = NSURLSession.sharedSession().dataTaskWithURL(siteUrl!) {(data,response,error) in
            if(error != nil){
                self.invalidUrlError()
            }
            dispatch_async(dispatch_get_main_queue(), {
                self.parseHtml(data)
                })
        }
        task.resume()
    }

    //Parse the data for images
    private func parseHtml(data:NSData?) {
        //unwrap the optional data
        if let html = data {
            //parse images, using Kanna
            if let doc = Kanna.HTML(html: html, encoding: NSUTF8StringEncoding) {
                for image in doc.css("img, link") {
                    if let imageLink = image["src"] {
                        //format the image link if needed. add the link to the arrary of links
                        if(imageLink.hasPrefix("http")){
                            imageLinks.append(imageLink)
                        }else {
                        imageLinks.append(siteAddress + "/" + imageLink)
                        }
                    }
                }
            }
        }
        getImages()
    }
    
    //load the images locally from the collection of image links
    func getImages() {
        for link in imageLinks {
            let url = NSURL(string: link)!
            if let data = NSData(contentsOfURL: url) {
                images.append(UIImage(data: data)!)
                imageCollection.reloadData()
            }
        }
        imageCollection.reloadData()
        activityIndicator.stopAnimating()
        activityIndicator.hidden = true
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = imageCollection.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ImageCollectionViewCell
        cell.image.image = images[indexPath.row]
        return cell
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func invalidUrlError(){
        let alert = UIAlertController(title: "Error", message: "Invalid URL", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func goButtonPushed(sender:UIButton) {
        dismissKeyboard()
        imageLinks.removeAll()
        images.removeAll()
        imageCollection.reloadData()
        if searchAddressTextField.text != "" {
            activityIndicator.hidden = false
            activityIndicator.startAnimating()
            siteAddress = "http://" + searchAddressTextField.text!
            setUpScrape()
        }
        else {
            invalidUrlError()
        }
    }
}

