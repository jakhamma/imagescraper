//
//  ImageCollectionViewCell.swift
//  
//
//  Created by brendan woods on 2016-07-16.
//
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image:UIImageView!
    
}
